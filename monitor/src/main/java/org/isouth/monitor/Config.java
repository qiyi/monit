package org.isouth.monitor;

import java.util.List;

public interface Config {

    Config getConfig(String name);

    String getString(String name);

    boolean getBoolean(String name);

    int getInt(String name, int defaultValue);

    List<Config> getConfigList(String name);

    List<String> getConfigNames();
}
