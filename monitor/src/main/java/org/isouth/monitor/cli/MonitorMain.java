package org.isouth.monitor.cli;

import java.io.File;
import java.io.UncheckedIOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.isouth.monitor.Config;
import org.isouth.monitor.Monitor;
import org.isouth.monitor.XMLConfig;
import org.w3c.dom.Document;

public class MonitorMain {

    public static void main(String[] args) throws Exception {
        Monitor monitor = new Monitor();
        List<Config> configs = loadConfig();
        monitor.init(configs);
        monitor.start();
    }

    /**
     * 当前只支持XML的 Parser
     *
     * @return
     * @throws Exception
     */
    private static Function<File, Config> createParser() throws Exception {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Function<File, Config> parser = (file) -> {
            try {
                Document includeDoc = db.parse(file);
                return new XMLConfig(includeDoc.getDocumentElement());
            } catch (Exception e) {
                throw new IllegalArgumentException(String.valueOf(file));
            }
        };
        return parser;
    }

    private static void collectConfig(File confFile, List<Config> configs, Function<File, Config> parser) {
        Config config = parser.apply(confFile);
        List<Config> includes = config.getConfigList("include");
        configs.add(config);
        if (!includes.isEmpty()) {
            File parent = confFile.getParentFile();
            for (Config include : includes) {
                String includePath = include.getString("path");
                if (includePath != null && !includePath.isEmpty()) {
                    File includeFile = new File(includePath);
                    if (!includeFile.isAbsolute()) {
                        includeFile = new File(parent, includePath);
                    }
                    collectConfig(includeFile, configs, parser);
                }
            }
        }
    }

    private static List<Config> loadConfig() throws Exception {
        String conf = System.getProperty("config.file", "conf/monitor.xml");
        File confFile = new File(conf);
        Function<File, Config> parser = createParser();
        List<Config> configs = new ArrayList<>();
        collectConfig(confFile, configs, parser);
        return configs;
    }

}
