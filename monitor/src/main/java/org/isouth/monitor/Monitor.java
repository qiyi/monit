package org.isouth.monitor;

import org.isouth.monitor.observer.AbstractObservable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.List;
import java.util.concurrent.*;

public class Monitor extends AbstractObservable<String> {
    public static final String JOB_ADD = "JOB_ADD";
    public static final String JOB_REMOVE = "JOB_REMOVE";
    private final Logger logger = LoggerFactory.getLogger(Monitor.class);
    private ServiceLoader<Action> actionLoader = new ServiceLoader<>(Action.class);
    private ConcurrentMap<String, Job> jobs = new ConcurrentHashMap<>();
    private ExecutorService executor;
    private ScheduledExecutorService scheduler;
    private volatile boolean started;

    public ScheduledFuture<?> schedual(Runnable runnable, int delay) {
        return scheduler.schedule(runnable, delay, TimeUnit.SECONDS);
    }

    public Future<?> submit(Runnable task) {
        return executor.submit(task);
    }

    public boolean isStarted() {
        return this.started;
    }

    private Action findAction(String name) throws Exception {
        Action action = actionLoader.getService(name);
        if (action == null) {
            logger.error("Find action {} failed.", name);
            throw new IllegalStateException("Action " + name + " not exist.");
        }
        return action;
    }

    private Action resolveAction(Job job, String name, Config config) throws Exception {
        Config actionConfig = config.getConfig(name);
        Action action = findAction(name);
        action.init(job, actionConfig);
        return action;
    }

    private Job buildJob(Config config) throws Exception {
        String name = config.getString("name");
        logger.debug("Start build job {}.", name);
        Job job = new Job(name).monitor(this);
        Action startAction = resolveAction(job, "start", config);
        job.startAction(startAction);
        Action stopAction = resolveAction(job, "stop", config);
        job.stopAction(stopAction);

        Config checkerConfigs = config.getConfig("checkers");
        if (checkerConfigs != null) {
            List<String> checkerNames = checkerConfigs.getConfigNames();
            for (String checkerName : checkerNames) {
                logger.debug("Resolve checker {}.", checkerName);
                Action checker = resolveAction(job, checkerName, checkerConfigs);
                job.checker(checker);
            }
        }
        logger.debug("Build job {} successfully.", name);
        return job;
    }

    public void init(List<Config> configs) throws Exception {
        logger.debug("Start init monitor.");
        // TODO 首先初始化 monitor 监听器
        for (Config config : configs) {
            List<Config> jobConfigs = config.getConfigList("job");
            for (Config jobConfig : jobConfigs) {
                Job job = buildJob(jobConfig);
                addJob(job);
            }
        }
    }

    public void addJob(Job job) {
        logger.debug("Add job {}", job);
        jobs.putIfAbsent(job.name(), job);
        fire(JOB_ADD, job);
    }

    public void delJob(String name) {
        Job job = jobs.remove(name);
        fire(JOB_REMOVE, job);
    }

    public Job getJob(String name) {
        return jobs.get(name);
    }

    public void startJob(Job job) {
        logger.debug("Start job {}", job);
        job.monitor(this);
        job.start();
    }

    public void stopJob(Job job) {
        logger.debug("Stop job {}.", job.name());
        job.stop();
    }

    private void fork() {
        Thread t = new Thread(() -> {
            logger.debug("Monitor thread started.");
            while (started) {
                try {
                    Thread.sleep(3600_000L);
                } catch (InterruptedException e) {
                    logger.debug("Monitor thread interrupted.");
                }
            }
            logger.debug("Monitor thread exit.");
        }, "monitor");
        t.start();

        logger.debug("Add shutdown hook.");
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            logger.debug("Receive shutdown signal.");
            Monitor.this.shutdown();
            t.interrupt();
        }, "monitor-shutdownhook"));
    }

    public void start() {
        logger.debug("Begin start monitor.");
        this.started = true;
        scheduler = Executors.newSingleThreadScheduledExecutor();
        executor = Executors.newCachedThreadPool();
        new HashSet<>(jobs.values()).forEach(this::startJob);
        fork();
        logger.debug("End start monitor");
    }

    public void shutdown() {
        logger.debug("Begin shutdown monitor.");
        this.started = false;

        this.scheduler.shutdown();
        this.executor.shutdown();

        try {
            this.scheduler.awaitTermination(10, TimeUnit.SECONDS);
        } catch (InterruptedException ie3) {
            logger.warn("Scheduler interrupted.");
        }
        try {
            this.executor.awaitTermination(1, TimeUnit.MINUTES);
        } catch (InterruptedException ie4) {
            logger.warn("Executor interrupted.");
        }

        logger.debug("Shutdown successfully.");
    }
}
