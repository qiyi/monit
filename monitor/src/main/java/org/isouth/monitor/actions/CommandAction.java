package org.isouth.monitor.actions;

import org.isouth.monitor.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.StringTokenizer;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class CommandAction implements Action {

    private final Logger logger = LoggerFactory.getLogger(CommandAction.class);
    private ProcessBuilder processBuilder;
    private String command;
    private int timeout;
    private Monitor monitor;

    protected String resolveCommand(Config config) {
        return config.getString("command");
    }

    private static final String[] parseCommand(String command) {
        StringTokenizer st = new StringTokenizer(command);
        String[] args = new String[st.countTokens()];
        for (int i = 0; st.hasMoreTokens(); i++) {
            args[i] = st.nextToken();
        }
        return args;
    }

    @Override
    public void init(Job job, Config config) {
        this.command = resolveCommand(config);
        String[] args = parseCommand(this.command);
        this.processBuilder = new ProcessBuilder(args);
        String chdir = config.getString("chdir");
        if (chdir != null) {
            this.processBuilder.directory(new File(chdir));
        }
        this.timeout = config.getInt("timeout", 30);
        this.processBuilder.redirectErrorStream(true);

        this.monitor = job.monitor();
    }

    @Override
    public void destroy(Job job) {
    }

    @Override
    public Result doIt(Job job) {
        Result result = new Result();
        Process process = null;
        try {
            process = this.processBuilder.start();
        } catch (IOException e) {
            logger.error("Execute command failed, command={}.", command, e);
            result.setCode(1);
            result.setContent(e);
        }

        if (process != null) {
            final Process p = process;
            final StringBuilder sb = new StringBuilder();

            // TODO 可能提交任务失败
            Future<?> f = this.monitor.submit(() -> {
                InputStream is = new BufferedInputStream(p.getInputStream());
                Reader reader = new BufferedReader(new InputStreamReader(is));
                int c;
                try {
                    while ((c = reader.read()) != -1) {
                        sb.append((char) c);
                    }
                } catch (IOException e2) {
                    logger.error("Read inputstream from process failed, command={}.", command, e2);
                    try {
                        reader.close();
                    } catch (IOException ignore) {
                    }
                }
                logger.debug("Command '{}' std/err output: {}", command, sb.toString());
            });

            try {
                if (p.waitFor(timeout, TimeUnit.SECONDS)) {
                    result.setCode(p.exitValue());
                    logger.debug("Command '{}' exit with exitValue '{}'", command, result.getCode());
                } else {
                    logger.warn("Command execute timeout with '{}' seconds.", command, timeout);
                    result.setCode(1);
                    p.destroyForcibly();
                }

                try {
                    // 获得进程std/err输出
                    f.get();
                } catch (Exception e) {
                    logger.error("Failed to get command '{}' std/err output.", command);
                }
                result.setContent(sb.toString());
            } catch (InterruptedException ine) {
                logger.error("Wait for process exit, but interrupted.");
                result.setCode(1);
                result.setContent(ine);
            }
        }

        logger.debug("Command result: {}", result);
        return result;
    }
}
