package org.isouth.monitor;

import org.isouth.monitor.observer.Observer;

/**
 * Created by qiyi on 5/2/2015.
 */
public interface Listener extends Observer {
}
