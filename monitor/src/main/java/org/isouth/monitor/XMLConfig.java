package org.isouth.monitor;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

public class XMLConfig implements Config {

    private Element element;

    public XMLConfig(Element element) {
        this.element = element;
    }

    @Override
    public Config getConfig(String name) {
        Element childElement = getChildElement(element, name);
        if (childElement != null) {
            return new XMLConfig(childElement);
        }
        return null;
    }

    @Override
    public String getString(String name) {
        if (element.hasAttribute(name)) {
            return element.getAttribute(name);
        } else {
            XMLConfig config = (XMLConfig) getConfig(name);
            if (config != null) {
                return config.element.getTextContent();
            }
        }
        return null;
    }

    @Override
    public boolean getBoolean(String name) {
        String value = getString(name);
        return Boolean.parseBoolean(value);
    }

    @Override
    public int getInt(String name, int defaultValue) {
        String strValue = getString(name);
        if (strValue == null) {
            return defaultValue;
        }
        return Integer.parseInt(getString(name));
    }

    @Override
    public List<Config> getConfigList(String name) {
        List<Element> childElements = getChildElements(element, name);
        List<Config> configs = new ArrayList<>(childElements.size());
        childElements.forEach(e -> configs.add(new XMLConfig(e)));
        return configs;
    }

    private static List<Element> getChildElements(Element parent, String name) {
        List<Element> childElements = new ArrayList<>();
        NodeList childNodes = parent.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            Node childNode = childNodes.item(i);
            if (childNode.getNodeType() == Node.ELEMENT_NODE) {
                Element childEle = (Element) childNode;
                if (name == null || childEle.getTagName().equals(name)) {
                    childElements.add(childEle);
                }
            }
        }
        return childElements;

    }

    private static Element getChildElement(Element parent, String name) {
        NodeList childNodes = parent.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            Node childNode = childNodes.item(i);
            if (childNode.getNodeType() == Node.ELEMENT_NODE) {
                Element childEle = (Element) childNode;
                if (childEle.getTagName().equals(name)) {
                    return childEle;
                }
            }
        }
        return null;
    }

    public List<String> getConfigNames() {
        List<Element> childElements = getChildElements(element, null);
        List<String> names = new ArrayList<>(childElements.size());
        childElements.forEach(childE ->
                        names.add(childE.getTagName())
        );
        return names;
    }
}
