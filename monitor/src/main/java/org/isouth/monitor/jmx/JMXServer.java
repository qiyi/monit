package org.isouth.monitor.jmx;

import org.isouth.monitor.Job;
import org.isouth.monitor.Monitor;

import static org.isouth.monitor.Monitor.*;

import org.isouth.monitor.observer.Observable;
import org.isouth.monitor.observer.Observer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by qiyi on 5/3/2015.
 */
public class JMXServer implements Observer<String, Observable<String>> {

    private final Logger logger = LoggerFactory.getLogger(JMXServer.class);

    public JMXServer() {
        // Register JMX Server
    }

    private void jobAdd(Monitor monitor, Job job) {

    }

    private void jobRemove(Monitor monitor, Job job) {

    }

    @Override
    public void receive(String subject, Observable<String> observable, Object news) {
        switch (subject) {
            case JOB_ADD:
                jobAdd((Monitor) observable, (Job) news);
                break;
            case JOB_REMOVE:
                jobRemove((Monitor) observable, (Job) news);
                break;
            default:
                logger.warn("Subject event ignored, subject={}, news={}", subject, news);
                break;
        }
    }

}
