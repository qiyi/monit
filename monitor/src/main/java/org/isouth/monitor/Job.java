package org.isouth.monitor;

import org.isouth.monitor.observer.AbstractObservable;
import org.isouth.monitor.observer.Observable;
import org.isouth.monitor.observer.Observer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Predicate;

public class Job extends AbstractObservable<String> {

    public static final String JOB_START = "JOB_START";

    public static final String JOB_STOP = "JOB_STOP";

    public enum Status {
        STOPPED, STARTING, RUNNING, STOPPING
    }

    public enum Mode {
        MANUAL, AUTO
    }

    private final Logger logger;

    private final String name;

    private Monitor monitor;

    private Action startAction;

    private Action stopAction;

    private Future<?> future;

    private Mode mode = Mode.AUTO;

    private Status status = Status.STOPPED;

    private Status expect = Status.STOPPED;

    private final Lock statusLock = new ReentrantLock(true);

    private final AtomicBoolean statusSignal = new AtomicBoolean();

    private final List<Action> checkers = Collections.synchronizedList(new ArrayList<>(1));

    public Job(String name) {
        this.name = name;
        this.logger = LoggerFactory.getLogger(Job.class.getName() + "[" + this.name + "]");
    }

    public void start() {
        logger.debug("Enter start function.");
        this.expect = Status.RUNNING;
        sendStatusSignal();
    }

    public void stop() {
        logger.debug("Stop job.");
        this.expect = Status.STOPPED;
        sendStatusSignal();
    }

    private void sendStatusSignal() {
        logger.debug("Start send status signal.");
        try {
            statusLock.lock();
            if (future == null || future.isDone() || future.cancel(false)) {
                // 任务还没有开始, 或者 成功取消了还没有开始执行的任务
                // 清空掉状态信号，然后直接触发状态事件
                logger.debug("The job has not started or has been canceled.");
                statusSignal.set(false);
                schedualStatusEvent(0);
            } else {
                logger.debug("The job has been executing, mark the signal.");
                // 记一个状态信号
                statusSignal.set(true);
            }
        } finally {
            statusLock.unlock();
        }
        logger.debug("Send status signal successfully.");
    }

    private void onStatusEvent() {
        logger.debug("Handle status event, expect={}, current={}.", expect, status);
        if (Status.STOPPED.equals(this.expect)) {
            // 期望状态为 STOPPED
            if (Status.RUNNING.equals(this.status)) {
                // 当前还在 RUNNING, 那么停止
                this.status = Status.STOPPING;
                stopJob();
                this.status = Status.STOPPED;
                fire(JOB_STOP, null);
            }
            schedualStatusEventOnSignal();
        } else if (Status.RUNNING.equals(this.expect)) {
            int delay = checkInterval();
            // 期望状态为 RUNNING
            if (Status.RUNNING.equals(this.status)) {
                // 当前已经是 RUNNING 状态
                if (!checkJob()) {
                    this.status = Status.STOPPING;
                    stopJob();
                    this.status = Status.STOPPED;
                    fire(JOB_STOP, null);
                }
            } else if (Status.STOPPED.equals(this.status)) {
                // 先做一次状态检查
                if (checkJob()) {
                    this.status = Status.RUNNING;
                    fire(JOB_START, null);
                }
            }

            if (Status.STOPPED.equals(this.status)) {
                // 当前 STOPPED 状态，启动
                this.status = Status.STARTING;
                if (startJob()) {
                    this.status = Status.RUNNING;
                    fire(JOB_START, null);
                } else {
                    this.status = Status.STOPPING;
                    stopJob();
                    this.status = Status.STOPPED;
                    fire(JOB_STOP, null);
                    delay = restartInterval();
                }
            }

            if (Mode.MANUAL.equals(mode) && Status.RUNNING.equals(this.status)) {
                // manual 模式，且已经启动好了， 只处理信号
                schedualStatusEventOnSignal();
            } else {
                schedualStatusEventWithSignal(delay);
            }
        }

        logger.debug("Handle status event successfully, expect={}, current={}.", expect, status);
    }

    /**
     * 只有在有状态信号时才调度任务事件
     */
    private void schedualStatusEventOnSignal() {
        try {
            statusLock.lock();
            if (statusSignal.getAndSet(false)) {
                logger.debug("Status signal exist while schedule signal");
                schedualStatusEvent(0);
            }
        } finally {
            statusLock.unlock();
        }
    }

    /**
     * 检查状态信号，并调度任务事件，如果有状态信号，那么会忽略指定的时延而直接调度任务事件
     *
     * @param delay 任务事件的时延
     */
    private void schedualStatusEventWithSignal(int delay) {
        try {
            statusLock.lock();
            if (statusSignal.getAndSet(false)) {
                logger.debug("Status signal exist while schedule signal");
                delay = 0;
            }
            schedualStatusEvent(delay);
        } finally {
            statusLock.unlock();
        }
    }

    private void schedualStatusEvent(int delay) {
        logger.debug("Schedual status event, delay={}", delay);
        if (!this.monitor.isStarted()) {
            logger.debug("The monitor has been shutdown, status event will be ignored.");
            return;
        }
        //TODO 可能提交任务失败
        future = this.monitor.schedual(() -> future = this.monitor.submit(this::onStatusEvent), delay);
    }

    public Job startAction(Action startAction) {
        this.startAction = startAction;
        return this;
    }

    public Job stopAction(Action stopAction) {
        this.stopAction = stopAction;
        return this;
    }

    public Job checker(Action checker) {
        this.checkers.add(checker);
        return this;
    }

    private int restartInterval() {
        return 10;
    }

    private int checkInterval() {
        return 20;
    }

    private boolean startJob() {
        logger.debug("Enter startJob function");
        Result result = this.startAction.doIt(this);
        return result.isSuccess();
    }

    private boolean stopJob() {
        logger.debug("Enter stopJob function.");
        Result result = this.stopAction.doIt(this);
        return result.isSuccess();
    }

    private boolean checkJob() {
        logger.debug("Enter checkJob function.");
        for (Action checker : checkers) {
            logger.debug("Start execute checker {}", checker);
            Result result;
            try {
                result = checker.doIt(this);
            } catch (Exception e) {
                logger.error("Checker {} execute failed with exception.", checker, e);
                result = new Result();
                result.setCode(1);
                result.setContent(e);
            }

            if (!result.isSuccess()) {
                logger.error("Checker {} execute failed, result={}", checker, result);
                return false;
            }
        }

        logger.debug("Check job ok.");
        return true;
    }

    public String name() {
        return name;
    }

    public Mode mode() {
        return mode;
    }

    public Status status() {
        return status;
    }

    public Status expect() {
        return expect;
    }

    public Job mode(Mode mode) {
        this.mode = mode;
        return this;
    }

    public Job monitor(Monitor monitor) {
        this.monitor = monitor;
        return this;
    }

    public Monitor monitor() {
        return this.monitor;
    }
}