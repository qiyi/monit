package org.isouth.monitor;

import java.awt.image.VolatileImage;

/**
 * Created by qiyi on 4/6/15.
 */
public class Result {
    private volatile boolean success = true;
    private volatile int code;
    private volatile Object content;

    public Result setCode(int code) {
        this.code = code;
        this.success = this.code == 0;
        return this;
    }

    public int getCode() {
        return this.code;
    }

    public boolean isSuccess() {
        return this.success;
    }

    public void setContent(Object content) {
        this.content = content;
    }

    public Object getContent() {
        return this.content;
    }

    @Override
    public String toString() {
        return "Result{" +
                "success=" + success +
                ", code=" + code +
                ", content=" + content +
                '}';
    }
}
