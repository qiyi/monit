package org.isouth.monitor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.URL;
import java.util.Enumeration;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * 服务加载类
 *
 * @param <T>
 */
public class ServiceLoader<T> {

    private final Logger logger = LoggerFactory.getLogger(ServiceLoader.class);

    private final Class<T> type;

    private final ClassLoader loader;

    private ConcurrentMap<String, Class<T>> cachedClasses = new ConcurrentHashMap<>();

    private ConcurrentMap<String, T> cachedInstances = new ConcurrentHashMap<>();

    private final Object shareServiceLock = new Object();

    private ConcurrentMap<String, String> cachedConfiguration = new ConcurrentHashMap<>();

    public ServiceLoader(Class<T> type) {
        this(type, null);
    }

    public ServiceLoader(Class<T> type, ClassLoader cl) {
        this.type = type;
        this.loader = (cl == null) ? ClassLoader.getSystemClassLoader() : cl;
        initialize();
    }

    private void initialize() {
        Properties p = new Properties();
        try {
            Enumeration<URL> resources = this.loader.getResources("META-INF/services/" + this.type.getName());
            while (resources.hasMoreElements()) {
                URL resource = resources.nextElement();
                Reader r = null;
                try {
                    InputStream is = resource.openStream();
                    r = new BufferedReader(new InputStreamReader(is, "utf-8"));
                    p.load(r);
                } catch (IOException ie) {
                    logger.error("Load resource {} failed.", resource, ie);
                    if (r != null) {
                        try {
                            r.close();
                        } catch (IOException ignore) {
                        }
                    }
                }
            }
        } catch (IOException e) {
            logger.error("Get resources for {} failed.", this.type, e);
        }
        p.stringPropertyNames().forEach(name->cachedConfiguration.put(name, p.getProperty(name)));
    }

    public T getService(String name) throws Exception {
        T t = cachedInstances.get(name);
        if (t != null) {
            return t;
        }

        Class<T> ct = cachedClasses.get(name);
        if (ct == null) {
            // 一些类包含静态初始化的资源，直到需要地时候才出加载类
            String clzName = cachedConfiguration.get(name);
            if (clzName == null) {
                // 不存在的类型
                return null;
            }
            cachedClasses.putIfAbsent(name, (Class<T>) this.loader.loadClass(clzName));
            ct = this.cachedClasses.get(name);
        }

        if (ct.isAnnotationPresent(Shareable.class)) {
            // 属于共享类型, 只加载一次
            synchronized (shareServiceLock) {
                t = cachedInstances.get(name);
                if (t != null) {
                    return t;
                }
                cachedInstances.put(name, ct.newInstance());
                t = cachedInstances.get(name);
            }
        } else {
            t = ct.newInstance();
        }
        return t;
    }
}
