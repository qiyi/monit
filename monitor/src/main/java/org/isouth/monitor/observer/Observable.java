package org.isouth.monitor.observer;

import java.util.function.Predicate;

/**
 * Created by qiyi on 4/16/2015.
 */
public interface Observable<T> {

    void fire(T subject, Object news);

    default void subscribe(Observer<T, Observable<T>> observer, T subject) {
        subscribe(observer, sub -> subject.equals(sub));
    }

    void subscribe(Observer<T, Observable<T>> observer, Predicate<T> filter);

    default void unSubscribe(Observer<T, Observable<T>> observer, T subject) {
        unSubscribe(observer, sub -> subject.equals(sub));
    }

    void unSubscribe(Observer<T, Observable<T>> observer, Predicate<T> filter);
}

