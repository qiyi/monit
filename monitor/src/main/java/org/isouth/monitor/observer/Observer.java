package org.isouth.monitor.observer;


import org.isouth.monitor.observer.Observable;

/**
 * Created by qiyi on 4/16/2015.
 */
@FunctionalInterface
public interface Observer<T, O extends Observable> {

    void receive(T subject, O observable, Object news);
}
