package org.isouth.monitor.observer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Predicate;

/**
 * Created by qiyi on 5/3/2015.
 */
public abstract class AbstractObservable<T> implements Observable<T> {

    protected final Map<Predicate<T>, List<Observer<T, Observable<T>>>> observerMap = new HashMap<>();

    protected final ReadWriteLock observerLock = new ReentrantReadWriteLock();

    @Override
    public void fire(T subject, Object news) {
        Lock lock = observerLock.readLock();
        try {
            lock.lock();
            observerMap.entrySet().stream().filter((entry) -> entry.getKey().test(subject)).forEach((e) -> {
                List<Observer<T, Observable<T>>> observers = e.getValue();
                if (observers != null && !observers.isEmpty()) {
                    observers.forEach((o) -> o.receive(subject, this, news));
                }
            });
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void subscribe(Observer<T, Observable<T>> observer, Predicate<T> filter) {
        Lock lock = observerLock.writeLock();
        try {
            lock.lock();
            List<Observer<T, Observable<T>>> observers = observerMap.get(filter);
            if (observers == null) {
                observerMap.put(filter, new ArrayList<>());
                observers = observerMap.get(filter);
            }
            observers.add(observer);
        } finally {
            lock.unlock();
        }
    }

    @Override
    public void unSubscribe(Observer<T, Observable<T>> observer, Predicate<T> filter) {
        Lock lock = observerLock.writeLock();
        try {
            lock.lock();
            List<Observer<T, Observable<T>>> observers = observerMap.get(filter);
            if (observers != null) {
                observers.remove(observer);
            }
        } finally {
            lock.unlock();
        }
    }
}
