package org.isouth.monitor;

public interface Action {

    void init(Job job, Config config);

    void destroy(Job job);

    Result doIt(Job job);
}